# Test Javascript 1

The idea with this test is to allow us to better comprehend the skills of candidates to developer jobs in regards to their knowledge of the Javascript language, it's runtime and ecosystem, in various experience levels.

This test should be completed by you on your own, in your own time. Take as much time as you need, but usually this test shouldn't take more than a couple of hours to be done.

## Delivery instructions

Download the instructions from this repo and, once done, put all project files in a .zip file and send it to `apply@nuuvem.recruitee.com`. You should also include instructions on how to execute the code you created, how to install any dependencies and how to run any tests, if necessary.

## Test descriptions

### 1. Prime Numbers

Implement a function `isPrime()` that receives an integer number and returns `true` or `false`, indicating whether the given number is prime. For example:

```javascript
isPrime(0)               // false
isPrime(1)               // false
isPrime(17)              // true
isPrime(10000000000000)  // false
```

Bonus questions:

- What is the time complexity of the `isPrime()` funtion as you wrote it?
- What if I asked you to return all prime number from zero up to the number supplied? How would you solve this? Would you re-use the `isPrime()` function? Why or why not? What is the time complexity of this new function? (you can implement this or just talk about your thought proccess)

### 2. Balanced String

Implement a function `isBalanced()` that takes a string and returns `true` or `false` indicating whether its round braces (parentheses) are balanced. For example:

```javascript
isBalanced(')(')                      // false
isBalanced('(()')                     // false
isBalanced('()()')                    // true
isBalanced('foo ( bar ( baz ) boo )') // true
isBalanced('foo ( bar ( baz )')       // false
isBalanced('foo ( bar ) )')           // false
```

Bonus: implement a new function, `isBalanced2()`, that does the same as you implemented above, but supports 3 types of braces: curly `{}`, square `[]`, and round `()`. For example:

```javascript
isBalanced2('(foo { bar (baz) [boo] })') // true
isBalanced2('foo { bar { baz }')         // false
isBalanced2('foo { (bar [baz] } )')      // false
```

### 3. Debugging existing code

#### 3.1 Greetings

I want this code to return "hey amy", but it is returning "hey arnold" - why?

```javascript
function greet (person) {
  if (person == { name: 'amy' }) {
    return 'hey amy'
  } else {
    return 'hey arnold'
  }
}
greet({ name: 'amy' })
```

Explain the problem and write a new code with the proposed solution.

#### 3.2 Barking dogs

I want my dog to `bark()`, but instead, I get an error. Why?

```javascript
function Dog (name) {
  this.name = name
}
Dog.bark = function () {
  console.log(this.name + ' says woof')
}
let fido = new Dog('fido')
fido.bark()
```

Explain the problem and write a new code with the proposed solution.

Bonus: is there more than one way to solve this? If so, what are the pros and cons of each solution?

### 4. Service Workers

This one is optional, but highly recommended.

Implement a web page that contains an `<img>` tag that point to an external image URL. Use a service worker so that when opening the page for the first time, the image is loaded and displayed, but it is also cached locally somehow, so that any subsequent accesses to the page now display the image from the local cache (even if the computer is offline).

Bonus points: how would you know when the image was updated, so you could not use the cached version anymore but instead request, display and cache the new version? (You can either implement this or just describe how you would do it)

Tip: you may assume this should only run on latest versions of Chrome and Firefox, on the desktop.

## Review

Your response will be evaluated by the following criteria:

1. Did you follow closely the test specification?
1. Your hability to clearly express your intent, both through code and text;
1. Quality of the code itself, how it's strutured and how it complies with good, modern Javascript practices;
1. Quality and coverage of unit / funcional / automated tests, if any;
1. Familiarity with the standard libraries of the language and other packages;
